package net.therap.helper;

/**
 * @author shadman
 * @since 11/22/17
 */
public class Helper {
    public static final String CONTENT_TYPE_HTML = "text/html";
    public static final String ERROR_MESSAGE = "An error occurred";
    public static final String HOME_URL = "/index.jsp";
    public static final String LOGIN_URL = "/static/public/login.jsp";
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";
}
