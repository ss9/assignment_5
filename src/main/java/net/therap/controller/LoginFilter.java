package net.therap.controller;

import net.therap.helper.Helper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/22/17
 */

@WebFilter(urlPatterns = {"/static/admin/*"})
public class LoginFilter implements Filter {

    public void destroy() {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpSession session = httpServletRequest.getSession();
            String name = ((String) session.getAttribute("username"));
            if (name != null && name.trim().equals(Helper.ADMIN_USERNAME)) {
                chain.doFilter(request, response);
            } else {
                if (response instanceof HttpServletResponse) {
                    ((HttpServletResponse) response).sendRedirect(((HttpServletRequest) request).getContextPath() +
                            "/LoginServlet");
                }
            }
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }
}
