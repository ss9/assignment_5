package net.therap.controller;

import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/22/17
 */

@WebServlet("/ErrorServlet")
public class ErrorServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().write(Helper.ERROR_MESSAGE);
        response.setContentType(Helper.CONTENT_TYPE_HTML);
        response.getWriter().write("<br><a href=" + request.getContextPath() + Helper.HOME_URL + ">Home</a>");
    }
}
