package net.therap.controller;

import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/22/17
 */

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (username != null && username.trim().equals(Helper.ADMIN_USERNAME) &&
                password != null && password.trim().equals(Helper.ADMIN_PASSWORD)) {
            request.getSession().setAttribute("username", "admin");
            response.sendRedirect(request.getContextPath() + Helper.HOME_URL);
        } else {
            response.sendRedirect(request.getContextPath() + "/ErrorServlet");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.LOGIN_URL);
    }
}
